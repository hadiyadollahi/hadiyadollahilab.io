+++
title = "هرچیزی میتونه جالب باشه حتی چیزی که ازش متنقری"
draft = false
description= ""
weight= 2
author= "هادی یدالهی"
+++

**هرچیزی میتونه جالب باشه حتی چیزی که ازش متنقری**

همیشه عادت داریم چیزایی رو ببینیم که واسمون جالب نیستن خودمم همینطوری هستم. مثلا یه درخت که چنتا برگ خشک داره جلب توجه میکنه این یه مثال بود یه اتفاقی که واسه خودم افتاده تو همین مایه‌ها هست: 

همیشه تو دوران تحصیل از تنها درسی که بدم میومد و ازش تنفر داشتم شبکه بود و اهمیتی بهش نمیدادم هرچند که بقیه درسام خوب بود ولی شبکه رو تو امتحانا خراب میکردم مفاهیم رو میفمیدم ولی تو امتحان بدترین نمره، همین شد مایه نفرت من حتی بهش فکر نمیکردم روزی گیرش بیفتم.

شرایط زندگی و مشکل چشم همه دست به دست هم دادن و من رو کشوندن سمت شبکه، کارم شد شبکه و تخصصم شد این هرچند اوایل ازش فرار می‌کردم ولی دیدم چاره‌ای ندارم اون داره دنبارم میاد 😁؛ باهاش کنار اومدم کم کم باهاش دوست شدم و دیدم به اون بدی هم که فکر میکردم نبود، چبزای جالبی داره که میتونی ازش لذت ببری.

از کارت از زندگی و هرچیز دیگه میشه لذت برد و واست جالب باشه حتی چیزایی که واست بدن و ازشون متنفری
