+++
title = "رزومه"
draft = false
description = "رزومه هادی یدالهی"
+++


فارغ التحصیل کارشناسی رشته نرم افزار کامپیوتر دانشکده فنی مهندسی شیراز(باهنر)

تدریس دوره‌های تخصصی نرم افزار و استاندارهای وزارت ارشاد در آموزشکده موج سپید ۱۳۹۰-۱۳۹۳

آزمونگر وزارت فرهنگ‌ و ارشاد اسلامی در رشته‌های کامپیوتر /هنر ۱۳۹۱-تا کنون

موسس انجمن کاربران لینوکس شیراز ۱۳۹۳

سردبیر سایت چهره و اخبار فناوری (فیسیت) ۱۳۹۳-۱۳۹۴

کارشناس فنی نمایندگی دیلینک (ایزی) در شیراز ۱۳۹۴-۱۳۹۵

کارشناس فروش فروشگاه شبکه برتر (نمایندگی دیلینک) ۱۳۹۵

مدیر فنی و مارکتینک شرکت ندای سبز ۱۳۹۵ تا کنون


**برخی فعالیت های جانبی**

سخنران گردهمایی مسئولین فاوای شهرداری شیراز در رابطه با مهاجرت به سمت نرم‌افزار آزاد

برگزاری اولین دوره آموزش برنامه نویسی به کودکان به همراه لاگ شیراز

مدیر همایش سفیر کتاب

سخنران همایش نرم افزار آزاد (برگزار کننده مدیریت فناوری اطلاعات و شبکه دولت استانداری)

